# -*- coding: utf-8 -*-
"""Setup file for refutils
"""
from setuptools import setup


setup(name='refutils',
      version='0.1',
      description='Utilities for HAWC reference models',
      url='https://gitlab.windenergy.dtu.dk/hawc-reference-models/reference-utilities',
      author='Jenni Rinker',
      author_email='rink@dtu.dk',
      license='MIT',
      packages=['refutils',  # top-level package
                ],
      install_requires=['wetb',  # wind energy toolbox (pip installation currently broken)
                        'requests'],
      zip_safe=False)
