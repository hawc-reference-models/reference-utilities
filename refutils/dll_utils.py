# -*- coding: utf-8 -*-
"""Download DLLs from control binary repository
"""
import os
import subprocess
import zipfile
import requests
from pathlib import Path
import sys
import shutil


def run(cmd, cwd='.', return_stdout=False):
    """Run a system command

    Parameters
    ----------
    cmd : string
        Command to run
    cwd : string, optional
        Working directory to run command from, default is current working directory
    return_stdout : bool, optional
        If True, the text output to stdout is return\n
        If False (default), the text is written on the screen
    """
    print("%s> %s" % (cwd, cmd), flush=True)
    cwd = Path(cwd).resolve()
    if cwd.is_file():
        cwd = cwd.parent
    stdout = [None, subprocess.PIPE][return_stdout]
    process = subprocess.Popen(cmd,
                               stdout=stdout,
                               stderr=subprocess.PIPE,
                               shell=True,
                               universal_newlines=True,
                               cwd=str(cwd))
    if return_stdout:
        stdout, stderr = process.communicate()
    else:
        process.wait()
        while True:
            out = process.stderr.read(1)
            if isinstance(out, bytes):
                out = out.decode()
            if out == '' and process.poll() is not None:
                break
            if out != '':
                sys.stderr.write(out)
                sys.stderr.flush()
        stdout = ""
        stderr = ""

    if process.returncode != 0:
        raise RuntimeError("Running command:\n%s> %s\nfailed:\n%s\n%s" % (cwd, cmd, stdout, stderr))
    return stdout.strip()


def clone(url, branch='master', depth=""):
    """Clone repository
    Parameters
    ----------
    url : string
        Url of repository to clone
    branch : string, optional
        Branch of repository to clone, default is master
    depth : int or ""
        Number of revisions to clone, default is all
    """
    folder = Path(os.path.splitext(os.path.basename(url))[0])
    if depth:
        depth = "--depth %d" % depth
    if folder.is_dir():
        if (folder / ".git").is_dir():
            run(f'git fetch --all {depth}', cwd=folder)
            run(f'git checkout {branch}', cwd=folder)
            run(f'git reset --hard origin/{branch}', cwd=folder)
            return
        else:
            shutil.rmtree(folder)
    run(f"git clone -b {branch} {depth} --single-branch {url}")


def clone_hawc2binary(platform='win32'):
    """Clone HAWC2 binaries, platform can be 'win32','win64','linux'"""
    p = f'hawc2-{platform}'
    clone("git@gitlab.windenergy.dtu.dk:HAWC2/hawc2-binary/%s.git" % p, depth=1)
    return Path("./%s/" % p)


def delete_dlls(folder):
    """Delete DLLs in a specific folder"""
    [os.remove(os.path.join(folder, f)) for f in os.listdir(folder) if
     f.endswith('dll')]


def clone_dll_repo(branch='master'):
    """Clone repo with binary dlls"""
    git_url = 'https://gitlab.windenergy.dtu.dk/OpenLAC/control-binary/control-win32.git'
    clone(git_url, branch, 1)


def _get_dll(url, folder='.', zipname='artifacts.zip'):
    """Get dll from url artifact and unzip"""
    r = requests.get(url)
    with open(zipname, 'wb') as f:
        f.write(r.content)
    with zipfile.ZipFile(zipname, 'r') as zip_ref:
        zip_ref.extractall(folder)
    os.remove(zipname)


def get_towerclearance_mblade(folder='.', version='1.0.0', bits=[32]):
    """Get towerclearance_mblade directly from repository"""
    url = ('https://gitlab.windenergy.dtu.dk/OpenLAC/servosandutilities/' +
           f'towerclearance-mblade/-/jobs/artifacts/v{version}/download?job=compile_dlls')
    _get_dll(url, folder)
    if 32 not in bits:
        os.remove(os.path.join(folder, 'towerclearance_mblade.dll'))
    if 64 not in bits:
        os.remove(os.path.join(folder, 'towerclearance_mblade_x64.dll'))


def get_nrel_discon_interface(folder='.', version='0.1.0', bits=[32]):
    """Get DISCON dll and interface dll directly from repository for NREL 5 MW.
    Deletes OC3Hywind DLLs, so does not work for floating OC models.
    """
    url = ('https://gitlab.windenergy.dtu.dk/OpenLAC/servosandutilities/' +
           f'nrel-5mw-discon/-/jobs/artifacts/v{version}/download?job=compile_dlls')
    _get_dll(url, folder)
    [os.remove(os.path.join(folder, f)) for f in os.listdir(folder) if 'OC3Hywind' in f]
    if 32 not in bits:
        os.remove(os.path.join(folder, 'DISCON.dll'))
        os.remove(os.path.join(folder, 'nrel_5mw_interface.dll'))
    if 64 not in bits:
        os.remove(os.path.join(folder, 'DISCON_x64.dll'))
        os.remove(os.path.join(folder, 'nrel_5mw_interface_x64.dll'))
