# -*- coding: utf-8 -*-
"""Import functions from other modules
"""
import subprocess
#from .hawc_wrappers import run_hawc2, run_h2s


def get_repo_info():
    """Get branch and commit of repo"""
    url_res = subprocess.run(['git', 'config', '--get', 'remote.origin.url'],
                             stdout=subprocess.PIPE)
    url = url_res.stdout.decode('utf-8').rstrip('\n')
    br_res = subprocess.run(['git', 'rev-parse', '--abbrev-ref', 'HEAD'],
                            stdout=subprocess.PIPE)
    branch = br_res.stdout.decode('utf-8').rstrip('\n')
    com_res = subprocess.run(['git', 'log', '--pretty=format:"%h\t%cD"', '-n', '1'],
                             stdout=subprocess.PIPE)
    commit, date = com_res.stdout.decode('utf-8').rstrip('\n').split('\t')
    commit = commit.lstrip('"')
    date = date.rstrip('"')
    return url, branch, commit, date
