# -*- coding: utf-8 -*-
"""Simple wrappers to call HAWC2 and H2S executables
"""
from pathlib import Path
import subprocess
from ._config import hawc2_paths, h2s_paths


def run_hawc2(htc, cwd='.', ver='12.8'):
    """Call HAWC2 on htc file"""
    cwd = Path(cwd).absolute().as_posix()
    htc = Path(htc).absolute().as_posix()
    # sanity checks
    try:  # check version is given in _config.py
        exe_path = hawc2_paths[ver]
    except KeyError:
        raise ValueError(f'HAWC2 version "{ver}" not defined in _config.py!')
    # check that executable exists
    if not Path(exe_path).exists():
        raise ValueError('HAWC2 exe not found at path given in _config.py!')
    # run HAWC2
    proc = subprocess.run([exe_path, htc], cwd=cwd, stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT)
    if proc.returncode:
        print(f'HAWC2 {ver} failed!!! Error message:\n')
        print(proc.stdout.decode('utf-8'))


def run_h2s(htc, cwd='.', ver='2.16a'):
    """Call H2S on htc file"""
    cwd = Path(cwd).absolute().as_posix()
    htc = Path(htc).absolute().as_posix()
    # sanity checks
    try:  # check version is given in _config.py
        exe_path = h2s_paths[ver]
    except KeyError:
        raise ValueError(f'H2S version "{ver}" not defined in _config.py!')
    # check that executable exists
    if not Path(exe_path).exists():
        raise ValueError('H2S exe not found at path given in _config.py!')
    # run H2S
    proc = subprocess.run([exe_path,  htc], cwd=cwd, stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT)
    if proc.returncode:
        print(f'H2S {ver} failed!!! Error message:\n')
        print(proc.stdout.decode('utf-8'))
