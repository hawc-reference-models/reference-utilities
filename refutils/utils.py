# -*- coding: utf-8 -*-
import os
import shutil
from wetb.hawc2 import HTCFile
from refutils.htc_conversion import base_to_hs2, base_to_step, base_to_turb, base_to_test
from refutils.dll_utils import (get_towerclearance_mblade, clone_dll_repo, 
                                get_nrel_discon_interface, run, clone_hawc2binary)
from refutils import get_repo_info


class ReferenceModel():
    """Class for collecting dlls, generate htc-input files and test the model


    Initialization parameters
    ----------
    model_path_name : string
        Name of model (should match folder name).
    htc_basename : string
        Base name for htc files.
    model_kwargs : dictionary
        Keyword arguments for the model.
    dll_list : list
        List of which DLLs to copy from the control repo.
    control_dll_branch : string, optional
        Which branch in the control repo to checkout to. Default is "ref_models".
    step : bool, optional
        Whether to generate a step-wind htc file. Default is true
    turb : bool, optional
        Whether to generate a turbulent htc file. Default is true.
    hs2 : bool, optional
        Whether to generate a hawcstab2 file. Default is true.
    test : bool, optional
        Whether to generate a test htc file. Default is false.
    """

    def __init__(self, model_path_name, htc_basename, model_kwargs, dll_list,
                 control_dll_branch='ref_models',
                 step=True, turb=True, hs2=True, test=False):
        self.model_path_name = model_path_name
        self.htc_basename = htc_basename
        self.model_kwargs = model_kwargs
        self.dll_list = dll_list
        self.control_dll_branch = control_dll_branch
        self.step = step
        self.turb = turb
        self.hs2 = hs2
        self.test = test

    def prepare_model(self):
        """Collect dlls, generate htc-input files and save working-model zip file"""

        # get ref name
        try:
            branch_name = run('git branch --show-current', return_stdout=True)
        except Exception:
            branch_name = 'unknown'

        print(f'Preparing zip file for {branch_name}...')
        # script inputs
        mod_dir = f'../{self.model_path_name}/'  # must end with slash
        zip_name = f'../{self.model_path_name}'

        # ======= make hawcstab2 and step-wind files =======
        htc_base = mod_dir + f'htc/{self.htc_basename}.htc'

        # turbulent file
        if self.turb:
            turb_path = mod_dir + f'htc/{self.htc_basename}_turb.htc'
            base_to_turb(htc_base, turb_path, **self.model_kwargs)

        # hawcstab2
        if self.hs2:
            hs2_path = mod_dir + f'{self.htc_basename}_hs2.htc'
            base_to_hs2(htc_base, hs2_path, **self.model_kwargs)

        # step wind
        if self.step:
            step_path = mod_dir + f'htc/{self.htc_basename}_step.htc'
            base_to_step(htc_base, step_path, **self.model_kwargs)

        if self.test:
            base_to_test(htc_base)

        # ======= create/clean control folder =======
        control_dir = mod_dir + 'control/'  # must end with slash!

        # delete dlls in control repo
        if os.path.isdir(control_dir):
            run('git clean -f', cwd=control_dir)
        else:
            os.mkdir(control_dir)

        # list of needed DLLs
        dll_list = [((dll, dll), dll)[isinstance(dll, tuple)] for dll in self.dll_list]
        dll_name_list = [dll for dll, _ in dll_list]  # names only

        # ======= download tower clearance DLL  =======
        if 'towerclearance_mblade.dll' in dll_list:
            get_towerclearance_mblade(folder=control_dir)

        # ======= download discon DLL  =======
        if 'DISCON.dll' in dll_name_list:
            get_nrel_discon_interface(folder=control_dir)

        # ======= get remaining dlls from binary repo =======

        # clone dll repo (will throw error if repo not deleted)
        clone_dll_repo(self.control_dll_branch)

        # copy dlls to control repo
        [shutil.copy('control-win32/' + t[0], control_dir + t[1])
            for t in dll_list if t[0] not in ['towerclearance_mblade.dll', 'DISCON.dll',
                                              'nrel_5mw_interface.dll']]

        # ======= write the git branch and commit to file =======
        url, branch, commit, date = get_repo_info()
        with open(mod_dir + 'git_version.txt', 'w') as f:
            f.write(f'Git info for {self.model_path_name}-{branch_name} zip file\n')
            f.write('------------------------------------\n')
            f.write('Automatically generated using CI on following repo.\n\n')
            f.write(f'Repo url: {url}\n')
            f.write(f'Branch: {branch}\n')
            f.write(f'Commit: {commit} made on {date}\n')

        # ======= make the archive =======
        shutil.make_archive(zip_name, 'zip', mod_dir)

    def test_with_hawc2binary_master(self):
        """Clone HAWC2-Binary and run test simulation. Exception is raise if simulation do not succeed"""
        if not os.path.isfile(f'../{self.model_path_name}/htc/{self.htc_basename}_test.htc'):
            self.test = True
            self.prepare_model()
        hawc2_path = clone_hawc2binary()
        htc = HTCFile(f'../{self.model_path_name}/htc/{self.htc_basename}_test.htc')
        stdout, log = htc.simulate(os.path.join(hawc2_path, 'HAWC2MB.exe'))
        print("test succeeded")
