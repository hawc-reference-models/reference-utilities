# -*- coding: utf-8 -*-
"""tests for dll utils
"""
import os
import subprocess
import sys
import pytest
from refutils.dll_utils import (get_towerclearance_mblade, delete_dlls, clone_dll_repo,
                                get_nrel_discon_interface)


@pytest.fixture
def clean_zips_dlls():
    """remove dlls and zips before and after test"""
    [os.remove(f) for f in os.listdir('.') if
      (f.endswith('.zip') or f.endswith('.dll'))]
    yield  # run the test
    [os.remove(f) for f in os.listdir('.') if
      (f.endswith('.zip') or f.endswith('.dll'))]


@pytest.mark.parametrize("bits", [(32,), (64,), (32, 64)])
def test_get_nrel_discon_interface(bits, clean_zips_dlls):
    """check download of discon and interface"""
    out_files = sorted([f'DISCON{"_x64"*(b == 64)}.dll' for b in bits] +
                       [f'nrel_5mw_interface{"_x64"*(b == 64)}.dll' for b in bits])
    get_nrel_discon_interface(bits=bits)
    local_files = sorted([f for f in os.listdir('.') if f.endswith('.dll')])
    assert out_files == local_files


@pytest.mark.parametrize("bits", [(32,), (64,), (32, 64)])
def test_get_towerclearance_mblade(bits, clean_zips_dlls):
    """check download of tower clearance"""
    out_files = sorted([f'towerclearance_mblade{"_x64"*(b == 64)}.dll' for b in bits])
    get_towerclearance_mblade(bits=bits)
    local_files = sorted([f for f in os.listdir('.') if f.endswith('.dll')])
    assert out_files == local_files


def test_delete_dlls():
    """verify dlls are deleted"""
    get_towerclearance_mblade(bits=[32])
    delete_dlls('.')
    assert 0 == len([f for f in os.listdir('.') if f.endswith('.dll')])


def test_clone_dll_repo():
    if 'win' in sys.platform:
        subprocess.run('RD /S /Q control-win32', shell=True)
    clone_dll_repo()
    assert 'control-win32' in os.listdir('.')
    if 'win' in sys.platform:
        subprocess.run('RD /S /Q control-win32', shell=True, check=True)
