# Reference Utilities

Python-script utilities for the HAWC Reference Models

## HTC file creation

The functions in `refutils.htc_conversion` convert a "base" htc file to a
variety of other formats (e.g., step wind, turbulent wind, etc.). The base
htc file is assumed to be a 100-sec simulation with steady wind.

Certain parameters for the creation of the new files may be passed in as
keyword arguments. Many other parameters are hard-coded and may differ based
on the `refutils` version. Check the source code to see what values in the
htc files are updated in which functions.

## Configuration of HAWC wrappers

To use `run_hawc2` and `run_h2s`, you need to make file `refutils/_config.py`
that defines dictionaries `hawc2_paths` and `h2s_paths` for all HAWC2 and
HAWC2S version executables of interest. An example of `refutils/_config.py`
could look like this:

```
# -*- coding: utf-8 -*-
"""Paths to HAWC2 and H2S executables
"""
hawc2_paths = {'12.4': 'C:/Users/rink/Documents/hawc2/HAWC2_all_12-4/HAWC2MB.exe',
               '12.5': 'C:/Users/rink/Documents/hawc2/HAWC2_all_12-5/HAWC2MB.exe',
               '12.6': 'C:/Users/rink/Documents/hawc2/HAWC2_all_12.6/HAWC2MB.exe',
               '12.8': 'C:/Users/rink/Documents/hawc2/HAWC2_12.8_1900/HAWC2MB.exe'}
h2s_paths = {'2.14': 'C:/Users/rink/Documents/hawc2/hawcstab2_v2.14/HAWC2S_x64.exe',
             '2.15': 'C:/Users/rink/Documents/hawc2/hawcstab2_v2.15/HAWC2S_x64.exe',
             '2.16a': 'C:/Users/rink/Documents/hawc2/hawcstab2_v2.16a/HAWC2S_x64.exe'}

```